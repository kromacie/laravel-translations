<?php namespace Kromacie\LaravelTranslations;

use Illuminate\Translation\FileLoader;

class TranslationsLoader extends FileLoader
{
    /**
     * Load the messages for the given locale.
     *
     * @param string $locale
     * @param string $group
     * @param string $namespace
     *
     * @return array
     */
    public function load($locale, $group, $namespace = null)
    {
        $default = parent::load($locale, $group, $namespace);

        if ($namespace !== '*' && !is_null($namespace)) {
            return $default;
        }

        $cached = $this->getCachedTranslations($locale, $group, $namespace);

        return array_replace_recursive($default, $cached);
    }

    public function getCachedTranslations($locale, $group, $namespace)
    {
        try {
            return Translation::getTranslationsForGroupAndLocale($group, $locale);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

}