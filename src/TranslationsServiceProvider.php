<?php namespace Kromacie\LaravelTranslations;

use Carbon\Carbon;
use Illuminate\Translation\TranslationServiceProvider as BaseTranslationServiceProvider;
use Illuminate\Translation\Translator;

class TranslationsServiceProvider extends BaseTranslationServiceProvider {


    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {

        $this->registerLoader();

        $this->mergeConfigFrom(
            __DIR__.'/../config/translations.php',
            'translations'
        );

        $this->app->singleton('translator', function($app)
        {
            // When registering the translator component, we'll need to set the default
            // locale as well as the fallback locale. So, we'll grab the application
            // configuration so we can easily get both of these values from there.
            $locale = $app['config']['app.locale'];

            $trans = new Translator(new TranslationsLoader($app['files'], $app['path.lang']), $locale);

            $trans->setFallback($app['config']['app.fallback_locale']);

            return $trans;
        });

    }

    public function boot()
    {
        if($this->app->runningInConsole())
        {
            $date = Carbon::now();

            $this->publishes([
                __DIR__ . '/../config/translations.php' => config_path('translations.php')
            ], 'config');

            $this->publishes([
                __DIR__.'/../database/migrations/create_translations_table.php.stub' => database_path("migrations/{$date->format('Y_m_d_His')}_create_translations_table.php"),
                __DIR__.'/../database/migrations/create_groups_table.php.stub' => database_path("migrations/{$date->format('Y_m_d_His')}_create_groups_table.php"),
            ], 'migrations');

        }
    }


}