<?php namespace Kromacie\LaravelTranslations;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{

    public $timestamps = false;

    protected $fillable = [
        'name'
    ];

    public function getTable()
    {
        return config('translations.groups_table');
    }

    public static function boot()
    {
        static::created(function (Group $group){
            $group->flushSelected($group->id);
        });
        static::updated(function (Group $group){
            $group->flushSelected($group->getOriginal('id'));
        });
        static::deleted(function (Group $group){
            $group->flushSelected($group->id);
        });
    }

    /**
     * @param $group
     * @throws \Exception
     */
    public static function flushSelected($group)
    {
        cache()->tags([self::getCacheTag(), $group])->flush();
    }

    public static function getCacheTag()
    {
        return config('translations.cache_key');
    }

    /**
     * @param $name
     * @throws \Exception
     */
    public static function deleteGroup($name)
    {
        /** @var Group $group */
        $group = self::where('name', '=', $name)->firstOrFail();
        $group->delete();
    }

    public static function updateGroup($old, $new)
    {
        /** @var Group $group */
        $group = self::where('name', '=', $old)->firstOrFail();
        $group->name = $new;
        $group->save();
        return $group;
    }

    public static function createGroup($name)
    {
        $group = new self();
        $group->fill(['name' => $name])->save();
        return $group;
    }


    /**
     * @throws \Exception
     */
    public static function flushCache()
    {
        cache()->tags(self::getCacheTag())->flush();
    }
}