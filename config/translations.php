<?php

return [

    'groups_table' => 'groups',

    'translations_table' => 'translations',

    'cache_key' => 'kromacie.translatable',

    'cache_expiration_time' => 60 * 60
];