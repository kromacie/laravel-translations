<?php namespace Kromacie\LaravelTranslations;

use Illuminate\Database\Eloquent\Model;

class Translation extends Model
{

    public $timestamps = false;

    public function getTable()
    {
        return config('translations.translations_table');
    }

    protected $fillable = [
        'locale',
        'group',
        'key',
        'value'
    ];

    public static function boot()
    {
        static::created(function (Translation $translation){
            $translation->flushSelected($translation->group, $translation->locale);
        });
        static::updated(function (Translation $translation){
            $translation->flushSelected($translation->getOriginal('group'), $translation->getOriginal('locale'));
        });
        static::deleted(function (Translation $translation){
            $translation->flushSelected($translation->group, $translation->locale);
        });
    }

    public static function createTranslation($group, $locale, $key, $value)
    {
        $translation = new self();
        $translation->group = $group;
        $translation->locale = $locale;
        $translation->key = $key;
        $translation->value = $value;
        $translation->save();
        return $translation;
    }

    public static function getByKey($key)
    {
        return self::where('key', '=', $key)->get();
    }

    public static function getByLocale($locale)
    {
        return self::where('locale', '=', $locale)->get();
    }

    public static function getByGroup($group)
    {
        return self::where('group', '=', $group)->get();
    }

    public static function getByLocaleAndGroup($locale, $group)
    {
        return self::where([
            ['locale', '=', $locale, 'AND'],
            ['group', '=', $group]
        ])->get();
    }

    public static function getByLocaleGroupAndKey($locale, $group, $key)
    {
        return self::where([
            ['locale', '=', $locale, 'AND'],
            ['group', '=', $group, 'AND'],
            ['key', '=', $key]
        ])->get();
    }

    /**
     * @param $group
     * @param $locale
     * @return mixed
     * @throws \Exception
     */
    public static function getTranslationsForGroupAndLocale($group, $locale)
    {
        try {
            return cache()->tags([self::getCacheTag(), $group, $locale])->remember('translations', config('translations.cache_expiration_time'), function () use ($group, $locale) {
                return self::query()->where([
                    ['group', '=', $group, 'AND'],
                    ['locale', '=', $locale]
                ])
                ->get(['key', 'value'])
                ->reduce(function ($array, Translation $translation){
                    array_set($array, $translation->key, $translation->value);
                    return $array;
                }) ?? [];
            });
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public static function getCacheTag()
    {
        return config('translations.cache_key');
    }

    /**
     * @param $group
     * @param $language
     * @throws \Exception
     */
    public static function flushSelected($group, $language)
    {
        try {
            cache()->tags([self::getCacheTag(), $group, $language])->flush();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @throws \Exception
     */
    public static function flushCache()
    {
        try {
            cache()->tags(self::getCacheTag())->flush();
        } catch (\Exception $e) {
            throw $e;
        }
    }
}