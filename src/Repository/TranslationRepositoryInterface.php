<?php namespace Kromacie\LaravelTranslations\Repository;

interface TranslationRepositoryInterface
{
    public function loadTranslations($locale, $group, $namespace);
}